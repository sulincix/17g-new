#include <stdio.h>
#include <lib17g.h>

// libcmd updater_function test
void write(char* line){
    printf("  line: %s",line);
}


// lib17g stage test
void stage1(){
    puts("  stage1 done");
}
void stage2(){
    puts("  stage2 done");
}

int main(){
    // libvalue test
    puts("TEST: libvalue");
    set_value("deneme","123");
    set_value("deneme2","321");
    get_value("deneme");
    puts("TEST: libvalue (PASS)");

    // libcmd test
    puts("TEST: libcmd");
    set_updater_function( (function) write);
    run_and_update("ls");
    puts("TEST: libcmd (PASS)");

    // lib17g test
    puts("TEST: lib17g");
    add_stage((function)stage1);
    add_stage_with_level((function)stage2,0);
    run_stages();
    //mount_source();
    //umount_source();
    rsync("../build/","../build/rsync/");
    get_total_inodes("/");
    char** disks = list_disks();
    int num = get_numof_disk();
    get_numof_partition(disks[num-1]);
    puts("TEST: lib17g (PASS)");
    // done
    return 0;
}
