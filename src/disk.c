#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <lib17g.h>

char **disks;
char** list_disks(){
  disks = malloc(sizeof(char*)*(get_numof_disk()));
  int i=0;
  DIR *dp;
  struct dirent *ep;
  dp = opendir ("/sys/block/");
  if (dp != NULL){
    while ((ep = readdir (dp)) != NULL){
        char* disk = ep->d_name;
        if (ep->d_name[0] != '.'){
            disks[i] = malloc(sizeof(char)*(strlen(disk)+6));
            strcpy(disks[i],"/dev/");
            strcat(disks[i],disk);
            i++;
        }
    }
    closedir (dp);
    return disks;
  }
}

int get_numof_disk(){
  int i=0;
  DIR *dp;
  struct dirent *ep;
  dp = opendir ("/sys/block/");
  if (dp != NULL){
    while ((ep = readdir (dp)) != NULL){
      if (ep->d_name[0] != '.'){
          i++;
      }
    }
    closedir (dp);
    return i;
  }
  return 0;
}

blkid_partition get_partition(char* disk, int num){
    blkid_probe pr;
    blkid_partlist ls;

    pr = blkid_new_probe_from_filename(disk);
    if (!pr){
        printf("faild to open device %s\n", disk);
        return NULL;
    }
    ls = blkid_probe_get_partitions(pr);
    blkid_partition par = blkid_partlist_get_partition(ls, num);
    blkid_free_probe(pr);
    return par;
}

int get_numof_partition(char* disk){
    blkid_probe pr;
    blkid_partlist ls;
    int nparts;

    pr = blkid_new_probe_from_filename(disk);
    if (!pr){
        printf("faild to open device %s\n", disk);
        return 0;
    }
    ls = blkid_probe_get_partitions(pr);
    if(!ls){
        return 0;
    }
    nparts = blkid_partlist_numof_partitions(ls);
    blkid_free_probe(pr);
    return nparts;
}
