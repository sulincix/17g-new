#include <sys/mount.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <lib17g.h>

void mount_source(){
    mkdir("/source",0755);
    run_printf("mount %s /source/", loop_device);
}

void umount_source(){
    system("umount /source -lf");
}

