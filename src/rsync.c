#include <lib17g.h>

void rsync(char* source, char* target){
    run_and_update_printf("rsync --verbose --archive --no-D --acls --hard-links %s* %s",source,target);
}
int get_total_inodes(char* target){
    char* isize = getoutput(build_string("df --inodes /%s | awk 'END{{ print $3 }}'",target));
    return atoi(isize);
}