#include <lib17g.h>
#define MAX_STAGES 1024
function stages[MAX_STAGES];
int percent = -1;

void add_stage_with_level(function stage, int level){
    while (stages[level] != NULL){
        level++;
    }
    stages[level] = stage;
}

void run_stages(){
    for(int i=0;i<1024;i++){
        if(stages[i] != NULL){
            stages[i]();
        }
    }
}

int get_percent(){
    return percent;
}

void set_percent(int p){
    percent = p;
}
