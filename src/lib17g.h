#ifndef _lib17g
#define _lib17g

#include <distro.h>

#include <libcmd.h>
#include <libvalue.h>
#include <blkid/blkid.h>


// stage.c
#define add_stage(stage) add_stage_with_level(stage, 0)
void add_stage_with_level(function stage, int level);
void run_stages();
int get_percent();
void set_percent(int p);

//mount.c
void mount_source();
void umount_source();

//rsync.c
void rsync(char* source, char* target);
int get_total_inodes(char* target);

//disk.c
char** list_disks();
blkid_partition get_partition(char* disk, int num);
int get_numof_partition(char* disk);
int get_numof_disk();
#endif
