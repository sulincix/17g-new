#ifndef _libcmd
#define _libcmd
#include <stddef.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
/* Function pointer type */
typedef int (*function)();

/* updater function
example:
  void write(char *line){
    ...
  }
  updater_function = (function) write;

 */


/* execute a command and run updater_function */
void run_and_update(char* command);
void run_and_update_printf(char* format, ...);
void set_updater_function(function u);
int run_printf(char* format, ...);
char* getoutput(char* command);

char* build_string(char* format, ...);
#endif
