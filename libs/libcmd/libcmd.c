#include <libcmd.h>

function updater_function;

void set_updater_function(function u){
    updater_function = u;
}

FILE* process;
void run_and_update(char* command){
    process = popen(command,"r");
    size_t len = 0;
    char *line = NULL;
    ssize_t read;
    if(process == NULL){
        return;
    }
    fprintf(stderr,"%s\n",command);
    while ((read = getline(&line, &len, process)) != -1) {
        if(updater_function != NULL){
            updater_function(line);
        }else{
            printf("%s\n",line);
        }
    }
}

char long_buffer[1024*1024*1024];
char medium_buffer[1024*1024];
char* getoutput(char* command){
    process = popen(command,"r");
    size_t len = 0;
    char *line = NULL;
    ssize_t read;
    strcpy(long_buffer,"");
    fprintf(stderr,"%s\n",command);
    if(process == NULL){
        return "";
    }
    while ((read = getline(&line, &len, process)) != -1) {
        strcat(long_buffer,line);
    }
    return long_buffer;
}

char* build_string(char* format, ...){
    strcpy(medium_buffer,"");
    va_list args;
    va_start (args, format);
    vsnprintf (medium_buffer, (1024*1024)-1, format, args);
    va_end (args);
    return medium_buffer;

}

int run_printf(char* format, ...){
    char cmd[1024];
    va_list args;
    va_start (args, format);
    vsnprintf (cmd, 1023, format, args);
    va_end (args);
    fprintf(stderr,"%s\n",cmd);
    return system(cmd);

}

void run_and_update_printf(char* format, ...){
    char cmd[1024];
    va_list args;
    va_start (args, format);
    vsnprintf (cmd, 1023, format, args);
    va_end (args);
    run_and_update(cmd);
}
