#ifndef _libvalue
#define _libvalue
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct value{
    char* name;
    char* value;
};

/* 
Get a value by name
if value is not exists return empty string.
*/
char* get_value(char* name);

/*
Add / Replace value by name and value
if value is already exists replace with new value.
*/
void set_value(char* name, char* value);
#endif
