#include <libvalue.h>

struct value *values;
int _len = 0;

char* get_value(char* name){
    // Check value and return.
    for(int i=0;i<_len;i++){
        if(strcmp(values[i].name, name)==0){
            return values[i].value;
        }
    }

    // if value is not exists return empty string
    return "";
}

void set_value(char* name, char* value){
    // Initial memory allocate
    if(_len == 0){
        values = malloc(sizeof(struct value));
    }
    // Check value and replace if exists
    for(int i=0;i<_len;i++){
        if(strcmp(values[i].name, name)==0){
            values[i].value=value;
            return;
        }
    }
    // generate new array struct and copy items
    int newsize = sizeof(struct value)*(1+_len);
    struct value *new_values = malloc(newsize);
    for(int i=0;i<_len;i++){
        new_values[i] = values[i];
    }

    // new item definition
    struct value v;
    v.name = name;
    v.value = value;

   // apply modified array
   new_values[_len] = v;
   _len++;
   values = new_values;
}

