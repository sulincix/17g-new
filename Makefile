DISTRO=debian
build: clean
	make -C libs build
	make -C src build DISTRO=$(DISTRO)

clean:
	rm -rf build

test: build
	make -C test build
	make -C test test
